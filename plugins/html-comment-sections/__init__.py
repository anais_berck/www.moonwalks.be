from readline import set_completion_display_matches_hook
from pelican import signals
import re


def split_sections(generator, content):
    """
    Split the markdown content of an article or page into sections
    everytime it encounter a comment of the form <!--[section-name]--> 
    and put those in a new `sections` attribute as a dictonnary.

    The `content` attribute is left intact.
    """

    html_content = content.content
    content.sections = {}

    pattern = re.compile(r"<!--\[((?:(?!\]-->).)*)\]-->((?:(?!<!--\[)[\s\S])+)")

    # find all matches to groups
    matches = pattern.finditer(html_content)

    for match in matches:
        # section name
        section_name = match.group(1)
        section_content = match.group(2)
        content.sections[section_name] = section_content

    if content.sections:
        sections_names = ", ".join([section_name for section_name in content.sections.keys()])
        print("Sections in {t}: {s}".format(t=content.title, s=sections_names))


def register():
    signals.article_generator_write_article.connect(split_sections)
    signals.page_generator_write_page.connect(split_sections)
