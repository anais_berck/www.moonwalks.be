from pelican import signals
import os
import json


# TODO: see of content.lang == lang (from splitted filename)

def add_multilang_json(generator, content):

    # print("Multilang JSON plugin")

    pathname = content.source_path
    (root, filename) = os.path.split(pathname)
    (basename_lang, ext) = os.path.splitext(filename)
    basename_split = basename_lang.split("_")
    basename = basename_split[0]
    lang = 'en' if len(basename_split) == 1 else basename_split[1]

    print("page '{p}' in lang '{l}'".format(p = basename, l=lang))

    ml_filename = basename + "_multilang" + ".json"
    ml_pathname = os.path.join(root, ml_filename)
    
    if os.path.isfile(ml_pathname):

        print("--> using {f} for translation".format(f = ml_filename))

        with open(ml_pathname, 'r') as file:
            ml_jsn = json.load(file)

            lang_jsn = { id: message[lang] for (id, message) in ml_jsn.items() }
            # print(json.dumps(lang_jsn, indent=4))
            # TODO: see if lang exist in json, print somethings otherwise

            content.multilang = lang_jsn


    # the global one
    ml_global_filename = "content/global_multilang.json"

    if os.path.isfile(ml_global_filename):
        with open(ml_global_filename, 'r') as file:
            ml_jsn = json.load(file)
            lang_jsn = { id: message[lang] for (id, message) in ml_jsn.items() }
            generator.context["multilang"] = lang_jsn

def register():

    signals.page_generator_write_page.connect(add_multilang_json)
    signals.article_generator_write_article.connect(add_multilang_json)
