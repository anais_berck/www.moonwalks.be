from pelican import signals
import os
from PIL import Image, ExifTags

# the images are copied to output direction because it has been added as:
# STATIC_PATHS = ['gallery']

def setup_gallery(generator):

    GALLERY = []

    # TODO: look at content settings to generate gallery_path
    IN_GALLERY_PATH = 'content/gallery/'
    OUT_GALLERY_PATH = '/gallery/'

    for filename in os.listdir(IN_GALLERY_PATH):

        img_data = {}
        img_data['src'] = OUT_GALLERY_PATH + filename
        img = Image.open(IN_GALLERY_PATH + filename)
        exif = {
            ExifTags.TAGS[k]: v
            for k, v in img._getexif().items()
            if k in ExifTags.TAGS
        }
        img_data['time'] = exif['DateTimeOriginal'].split(" ")[1]
        GALLERY.append(img_data)

    GALLERY = sorted(GALLERY, key=lambda item: item['time'])
    print("GALLERY plugin: parsed src and exifs for {n} image in {f}".format(n = len(GALLERY), f = IN_GALLERY_PATH))

    # adding it to the pelican context
    generator.context['GALLERY'] = GALLERY


def register():
    signals.static_generator_finalized.connect(setup_gallery)
    # signals.page_generator_write_page.connect(setup_gallery)
