

//  MOON COMPUTATION
//  ==========================================================================

function updateCurrentMoonWatch(date){

  let lat = window.walk_calendar["latitude"];
  let long = window.walk_calendar["longitude"];

  let moon = getMoonStateFromDate(date, lat, long);
  let formated_date = format_date_time(date);
  
  let lang = window.current_local.split("-")[0];
  let phase_name = moon["name_"+lang];
  phase_name = phase_name.split(" ").join(" <br>");

  // sunset
  // let distance = moon["distance"];
  // let sun_calc = SunCalc.getTimes(date, lat, long);
  // let sunset = format_date_time(sun_calc["sunset"])[2];

  // --- PRIMARY INFO ---
  let $watch = $('#moon-watch-section');
  $watch.find('.phase-name').html(phase_name);
  $watch.find('.date').html(formated_date['date']);

  // scheme position
  let angle = mapValue(moon["phase"], 0, 1, 0, 360);
  $(".watch__scheme .svg-moon").css("--moon-rotation", angle + "deg");

  // change the font
  updateAxis($watch, moon["phase"]);
}

function printAllMoonphases(){

  let langs = ['en', 'fr', 'nl', 'es'];
  
  for (let lg of langs) {
    
    console.log(lg);

    let phase_number = 0;
    for (let phase of MOONPHASES){

      let phase_name = phase["name_"+lg];

      let title = $('<h2>').html(phase_name);
      updateAxis(title, phase_number, true);
      $(".all-phases").append(title);

      phase_number = phase_number + 0.125;
    }
    $(".all-phases").append( 
      $('<hr>')
    );
  }
}

function toggleTheme(now){

  let lat = window.walk_calendar["latitude"];
  let long = window.walk_calendar["longitude"];
  let timezone = window.walk_calendar["timezone"];

  // we want to have the time now in "Europe/Brussels"
  let date = luxon.DateTime.fromISO(now.toISOString()).setZone(timezone);
  
  let [sunrise, sunset] = getSunStateFromDate(date, lat, long, timezone);
  
  console.log("now", date.toString());
  console.log("rise", sunrise.toString());
  console.log("set", sunset.toString());

  if( date < sunset && sunrise < date ){
    console.log("day");
  }
  else{
    console.log("night");
    $("body").addClass("night");
  }

}

function updateCurrentMoonTypo(date){
  let lat = window.walk_calendar["latitude"];
  let long = window.walk_calendar["longitude"];
  let moon = getMoonStateFromDate(date, lat, long);

  updateAxis($(".moon-phased-typo > p"), moon["phase"], false);
  updateAxis($(".moon-phased-typo > blockquote"), moon["phase"], false);
  updateAxis($(".moon-phased-typo > h2"), moon["phase"], true);
  updateAxis($("#about-section .about-box p:first-child"), moon["phase"], true);
}


$(document).ready(function(){

  // test zone for other date
  // first new moon of 2000 according to:
  // https://www.calendar-12.com/moon_phases/2000
  // let now = new Date(Date.parse('06 Jan 2000 19:14:00 GMT+1'));
  // let now = new Date(Date.parse('13 Jan 2000 05:41:00 GMT+1'));
  // let now = new Date(Date.parse('21 Jan 2000 05:41:00 GMT+1'));
  let now = new Date();

  toggleTheme(now);
  window.setInterval(function(){
    // now.setHours(now.getHours() + 2);
    updateCurrentMoonWatch(now);
    updateCurrentMoonTypo(now);
  }, 100);

});
