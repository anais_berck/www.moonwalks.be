Title: Calendrier
slug: index
lang: fr
template: index

<!--[intro]-->

Se promener lorsque toutes les couleurs disparaissent de la forêt est une expérience particulière.
Vous êtes à l'écoute d’autres sens. Vous découvrez le silence, mais vous remarquez aussi des mouvements et des sons qui sont absents pendant la journée.
De plus, lorsque vous marchez pendant les moments les plus intenses et les plus importants du cycle lunaire, vous pouvez transformer vos intentions. C'est à cela que vous invitent ces marches.

Nous marcherons en silence, entrecoupés de quelques expériences auxquelles An vous invite. Chaque promenade dure au maximum 2 heures. Nous terminons en partageant un thé.

<!--[bio]-->

Les balades sont une initiative d'[Anaïs Berck](https://www.anaisberck.be), représentée ici par les milliers d'arbres de la forêt de Soignes et par l'artiste, conteuse et guide nature An Mertens.
