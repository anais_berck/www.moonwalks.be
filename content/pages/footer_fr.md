Title: Footer
lang: fr

La typographie de ce site web varie légèrement sous l'influence de la lune. <br/>
Le thème de couleurs de ce site web change selon s'il fait jour ou nuit à Bruxelles.

Font [Unoverso](https://github.com/oxomoto/Unoverso-Libre) par Oxomoto, et Inconsolata Variable par Raph Levien.
Tous les textes et images sont publiés sous [license CC4R](https://constantvzw.org/wefts/cc4r.en.html), Anaïs Berck, 2022.
Conception et développement du site web par Doriane Timmermans ([OSP](http://osp.kitchen/)).