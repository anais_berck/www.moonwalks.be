Title: Footer
partial: true
save_as: 


The typography of this website varies slightly under the influence of the moon. <br/>
The website color theme changes following day time or night time in Brussels.

Font [Unoverso](https://github.com/oxomoto/Unoverso-Libre) by Oxomoto, and Inconsolata Variable by Raph Levien.
All texts and images are published under de [CC4R license](https://constantvzw.org/wefts/cc4r.en.html), Anaïs Berck, 2022.
Website design and development by Doriane Timmermans ([OSP](http://osp.kitchen/))