Title: Footer
lang: nl

De typografie van deze website varieert lichtjes onder invloed van de maan.  <br/>
Het kleurenthema van de website verandert naargelang het in Brussel dag- of nachttijd is.

Font [Unoverso](https://github.com/oxomoto/Unoverso-Libre) van Oxomoto, en Inconsolata Variable van Raph Levien.
Alle teksten en beelden zijn gepubliceerd onder de [CC4R licentie](https://constantvzw.org/wefts/cc4r.en.html), Anaïs Berck, 2022.
Website ontwerp en ontwikkeling door Doriane Timmermans ([OSP](http://osp.kitchen/)).