Title: Kalender
slug: index
lang: nl
template: index

<!--[intro]-->

Wandelen wanneer alle kleurtjes uit het bos verdwijnen, is een bijzondere ervaring.
Je stelt in op andere zintuigen. Je ontdekt de stilte, maar je merkt ook bewegingen en geluiden op die overdag afwezig zijn.
Wanneer je bovendien wandelt tijdens de meest intense en belangrijke momenten van de maancyclus, kan je je intenties transformeren. Dat is waartoe deze wandelingen je uitnodigen.

We wandelen in stilte, afgewisseld met enkele experimenten waartoe An je uitnodigt. Elke wandeling duurt maximum 2 uur. We sluiten af met een kopje thee.

<!--[bio]-->

De wandelingen zijn een initiatief van [Anaïs Berck](https://www.anaisberck.be), hier vertegenwoordigd door de duizenden bomen van het Zoniënwoud en door kunstenaar, verteller en natuurgids An Mertens.
