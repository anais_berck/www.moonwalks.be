Title: Calendar
save_as: index.html
slug: index
template: index

<!--[intro]-->

Walking when all the colours disappear from the forest is a special experience.
You tune in to other senses.
You discover silence, but you also notice movements and sounds that are absent during the day. Moreover, when you walk during the most intense and important moments of the lunar cycle, you can transform your intentions.
That is what these walks invite you to do.

We will walk in silence, interspersed with some experiments to which An invites you.
Each walk takes a maximum of 2 hours.
We conclude with a cup of tea.

<!--[bio]-->

The walks are an initiative of [Anaïs Berck](https://www.anaisberck.be), represented here by the thousands of trees of the Sonian forest and by artist, storyteller and nature guide An Mertens.
