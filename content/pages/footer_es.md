Title: Footer
lang: es

La tipografía de este sitio web varía ligeramente bajo la influencia de la luna.  <br/>
El tema del sitio web cambia de color según sea de día o de noche en Bruselas.

Fuente [Unoverso](https://github.com/oxomoto/Unoverso-Libre) de Oxomoto, e Inconsolata Variable de Raph Levien.
Todos los textos e imágenes están publicados bajo licencia [CC4R](https://constantvzw.org/wefts/cc4r.en.html), Anaïs Berck, 2022.
Diseño y desarrollo del sitio web por Doriane Timmermans ([OSP](http://osp.kitchen/)).