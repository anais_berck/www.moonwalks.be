Title: Calendario
slug: index
lang: es
template: index

<!--[intro]-->

Caminar cuando todos los colores desaparecen del bosque es una experiencia especial.
Se sintoniza con otros sentidos. Descubres el silencio, pero también percibes movimientos y sonidos que son ausentes durante el día.
Además, cuando caminas en los momentos más intensos e importantes del ciclo lunar, puedes transformar tus intenciones. A eso te invitan estos paseos.

Caminaremos en silencio, intercalando algunos experimentos a los que An te invita. Cada paseo dura un máximo de 2 horas. Concluimos con un té.

<!--[bio]-->

Los paseos son una iniciativa de [Anaïs Berck](https://www.anaisberck.be), representada aquí por los miles de árboles del Sonian Forest y por la artista, contadora y guía de la naturaleza An Mertens.
