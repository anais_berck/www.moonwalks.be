Title: flyer
template: flyer
status: hidden


<!--[en]-->

Walking when all the colours have disappeared from the forest is a special experience.
You tune in to other senses. You discover silence, but you also notice movements and sounds that are absent during the day. 
Moreover, when you walk during the most intense and important moments of the lunar cycle, you can transform your intentions.
That is what these walks invite you to do.

<!--[fr]-->

Se promener lorsque toutes les couleurs ont disparu de la forêt est une expérience particulière.
Vous êtes à l'écoute d’autres sens. Vous découvrez le silence, mais vous remarquez aussi des mouvements et des sons qui sont absents pendant la journée.
De plus, lorsque vous marchez pendant les moments les plus intenses et les plus importants du cycle lunaire, vous pouvez transformer vos intentions. C'est à cela que vous invitent ces marches.

<!--[nl]-->

Wandelen wanneer alle kleurtjes uit het bos verdwenen zijn, is een bijzondere ervaring.
Je stelt in op andere zintuigen. Je ontdekt de stilte, maar je merkt ook bewegingen en geluiden op die overdag afwezig zijn.
Wanneer je bovendien wandelt tijdens de meest intense en belangrijke momenten van de maancyclus, kan je je intenties transformeren. Dat is waartoe deze wandelingen je uitnodigen.