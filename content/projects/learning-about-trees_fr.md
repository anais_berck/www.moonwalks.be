Title: Reconnaître les Arbres
slug: learning-about-trees
lang: fr

Au cours de chacune de ces promenades, vous ferez connaissance avec quatre arbres différents. Vous apprendrez à observer leur forme, leur écorce, leurs feuilles et leurs fruits. Vous découvrirez également quels sont leurs pouvoirs médicinaux et spirituels et à quoi iels servent concrètement.

À la fin de la promenade, tu auras suffisamment d'outils pour rencontrer tes propres arbres-ami.es.

## Guide
An Mertens est guide nature et artiste.

## Pratique
Il est recommandé de réserver une série de 4 promenades, mais vous pouvez également vous inscrire à chaque promenade séparément.

Une promenade dure 2 heures et coûte 20€.

Une série de quatre promenades coûte 70€.

Nous nous rencontrons sur le parking d'Enfants Noyés/Verdronken Kinderen, à la Drève du Comte/Graafdreef à Boitsfort/Bosvoorde : [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Veuillez vous inscrire à l'avance sur anais@anaisberck.be.

Dates :

Série 1:
* Mercredi 4 septembre, de 18h à 20h
* Lundi 9 septembre, de 18h à 20h
* Lundi 16 septembre, de 18h à 20h
* Lundi 23 septembre, de 18h à 20h

Série 2:
* Lundi 30 septembre, de 17h à 19h
* Lundi 7 octobre, de 17h à 19h
* Lundi 14 octobre, de 17h à 19h
* Lundi 21 octobre, de 17h à 19h
