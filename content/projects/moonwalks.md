Title: Moonwalks
slug: moonwalks

<!--[main]-->

## New rituals
Botanist and Native American Indian Robin Wall Kimmerer states in her book “Braiding Sweetgrass” that the core of the climate crisis is related to losing the connection with the land. By reconnecting, a form of recovery of the land and its people can take place, as well as a sustainable caring for. The connection can be made in various ways, for example by taking responsibility for a piece of land, be it as a vegetable garden, ornamental garden or forest. Her hypothesis is that a person's physical, emotional and spiritual health improves greatly when the relationship with the land is reinstated in his/her life. She urges the creation of new rituals that promote such relationships. The moon walks try to be such a new ritual.

## Polyocene
Psychologist and philosopher Vinciane Despret, known for her books “Autobiographie d'un Poulpe” and “Habiter en oiseau”, proposes to no longer speak of the Anthropocene, which carries a dystopian connotation, but of the Polyocene, the proposal to start living from the idea that we as humans do not live alone on this planet. We are part of a polyphony, and we need to ask ourselves how we can better live together, and even more, create together with the more-than-human beings. It is important to de-centre our gaze so that we can create such relationships. The forest helps with this.

## Brussels
These are just two of the authors who specifically point to the end of anthropocentrism, and also to the personal well-being that we can develop if we dare to enter into relationships with non-human beings. In Brussels, we are lucky to have a large forest close by, the Sonian Forest. Moreover, it is not just any forest. It has always been a forest since the last ice age more than 10000 years ago. And even though the trees were replanted by Zinner in the 18th century, the forest still retains that special energy that characterises places where trees are the main inhabitants.

## Full and New Moonwalks
These and other authors inspired me to organise - as a new ritual - new and full moon walks. I have been doing this since the spring of 2021. They are walks that combine elements from forest bathing, meditation and guided nature walks.


<!--[quotes]-->

> I enjoyed the mixture of different ingredients: biology, fiction, politics... I also liked the silence, and the intensity of the experience. The space, for example, seemed very vast to me (I know the place well, I know we didn't walk miles and yet it seemed long, dense: a journey). I also found it very nice to see/feel the night appearing, and to realise that I was not afraid. Because I was with you? Because the trees had given me a friendly welcome?
>
> Rafaella

<!--  -->

> What I have kept as strong memories are the shapes and colours that changed during the walk, and also the sounds: our footsteps in silence on the gravel, and the birds. We both cherish the intention we set during the moment you dedicated to the new moon. It was strong. We think it is important to keep that alive.
>
> Doriane & Zeste

<!--  -->

> I was coming out of a very busy day and the walk calmed me down. In the beginning I had trouble concentrating on what the invitations were, because my head was still processing all the impressions of the day, but towards the end I was totally into it.
>
> Delphine

<!--  -->

> There was something that touched me and still does today, but I don't know how to explain it... probably the talking to the trees, our big brothers.
>
> Aicha
