Title: Paseos lunares
slug: moonwalks
lang: es

## Nuevos rituales
La botánica e india americana Robin Wall Kimmerer afirma en su libro «Braiding Sweetgrass» que el núcleo de la crisis climática está relacionado con la pérdida de la conexión con la tierra. Al reconectar, puede producirse una forma de recuperación de la tierra y su gente, así como un cuidado sostenible. La conexión puede hacerse de varias maneras, por ejemplo, responsabilizándose de un trozo de tierra, ya sea un huerto, un jardín ornamental o un bosque. Su hipótesis es que la salud física, emocional y espiritual de una persona mejora enormemente cuando se restablece la relación con la tierra en su vida. Insta a crear nuevos rituales que promuevan esas relaciones. Los paseos lunares intentan ser un nuevo ritual.

## Polyocene
La psicóloga y filósofa Vinciane Despret, conocida por sus libros «Autobiographie d'un Poulpe» y «Habiter en oiseau», propone que ya no se hable del Antropoceno, que tiene una connotación distópica, sino del Polioceno, la propuesta de empezar a vivir desde la idea de que los humanos no vivimos solos en este planeta. Somos parte de una polifonía, y tenemos que preguntarnos cómo podemos convivir mejor, y más aún, crear junto a los seres más-que-humanos. Es importante descentrar nuestra mirada para poder crear esas relaciones. Vivir experiencias en el bosque ayuda a ello.

## Bruselas
Estos son sólo dos de los autores que apuntan específicamente al fin del antropocentrismo, y también al bienestar personal que podemos desarrollar si nos atrevemos a entrar en relación con los seres más que humanos. En Bruselas, tenemos la suerte de contar con un gran bosque cerca, el Sonian Forest. Además, no es un bosque cualquiera. Siempre ha sido un bosque desde la última Edad de Hielo, hace más de 10000 años. Y aunque los árboles fueron replantados por Zinner en el siglo XVIII, el bosque sigue conservando esa energía especial que caracteriza a los lugares donde los árboles son los principales habitantes.

## Paseos de luna llena y nueva
Estos y otros autores me inspiraron a organizar -como un nuevo ritual- paseos de luna nueva y llena. Llevo haciéndolo desde la primavera de 2021. Son paseos que combinan elementos de baños de bosque, meditación y paseos guiados por la naturaleza.


<!--quotes-->


> Me gustó la mezcla de diferentes ingredientes: biología, ficción, política... También me gustó el silencio, y la intensidad de la experiencia. El espacio, por ejemplo, me pareció muy vasto (conozco bien el lugar, sé que no hicimos kilómetros y, sin embargo, me pareció largo, denso: un viaje). También me pareció muy agradable ver/sentir la aparición de la noche, y darme cuenta de que no tenía miedo. ¿Porque estaba con vosotros? ¿Porque los árboles me habían dado una bienvenida amistosa?
>
> Rafaella

<!--  -->

> Lo que he guardado como fuertes recuerdos son las formas y los colores que cambiaron durante el paseo, y también los sonidos: nuestras pisadas en silencio sobre la grava, y los pájaros. Les dos guardamos la intención que pusimos durante el momento que le dedicaste a la luna nueva. Fue muy fuerte. Creemos que es importante mantenerla viva.
>
> Doriane & Zeste

<!--  -->

> Salí de un día muy ajetreado y el paseo me calmó. Al principio me costó concentrarme en lo que eran las invitaciones, porque mi cabeza aún estaba procesando todas las impresiones del día, pero hacia el final estaba totalmente metida en ello.
>
> Delphine

<!--  -->

> Hubo algo que me conmovió y lo sigue haciendo hoy, pero no sé cómo explicarlo... probablemente haber hablado con los árboles, nuestros hermanos mayores.
>
> Aicha
