Title: Eetbaar Bos
slug: edible-forest
lang: nl

Tijdens deze wandeling ontdek je allerlei lekkers dat in de natuur te vinden is, zomaar langs een pad, in een boom of bij een struik. Van de kleinste planten tot de grootste bomen, er is van alles te vinden in het bos dat eet- en drinkbaar is. Het Zoniënwoud is beschermd gebied, daarom is plukken er niet bij.

Maar overvloed des te meer!

## Gids
An Mertens is natuurgids en kunstenaar. Ze organiseerde al eerder wandelingen rond Eetbaar Bos voor GC Wabo.

## Praktisch
Een wandeling duurt 2 uur en kost 20€.

We spreken af op de parking van Verdronken Kinderen, in de Graafdreef in Bosvoorde: [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Graag op voorhand inschrijven via anais@anaisberck.be.

Data

donderdag 5 september, 19u-21u

donderdag 19 september, 18u-20u

donderdag 3 oktober, 17u-19u

donderdag 10 oktober, 17u-19u
