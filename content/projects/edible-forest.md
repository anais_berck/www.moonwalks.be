Title: Edible Forest
slug: edible-forest

On this walk, you will discover all kinds of goodies found in nature, just along a path, in a tree or by a bush. From the smallest plants to the biggest trees, there are many beings in the forest ready to share edible and drinkable parts of their body. The Sonian Forest is a protected area, so picking is out of the question.

But abundance all the more!

## Guide
An Mertens is a nature guide and artist. She has previously organised walks around Edible Forest for GC Wabo.

## Practical
A walk lasts 2 hours and costs 20€.

We meet at the parking lot of Enfants Noyés/Verdronken Kinderen, in Drève du Comte/Graafdreef in Boisfort/Bosvoorde: [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Please register in advance at anais@anaisberck.be.

Dates

Thursday, September 5, 7pm-9pm

Thursday 19 September, 6-8pm

Thursday 3 October, 5pm-7pm

Thursday 10 October, 5pm-7pm
