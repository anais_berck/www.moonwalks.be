Title: Balades lunaires
slug: moonwalks
lang: fr

<!--[main]-->

## Nouveaux rituels
La botaniste et amérindienne Robin Wall Kimmerer affirme dans son livre «Les herbes sacrées» que le noyau de la crise climatique est lié à la perte du lien avec la terre. En se reconnectant, une forme de récupération de la terre et de ses habitants peut avoir lieu, ainsi qu'une prise de soin durable. Le lien peut être établi de différentes manières, par exemple en prenant la responsabilité d'un terrain, qu'il s'agisse d'un potager, d'un jardin d'ornement ou d'une forêt. Son hypothèse est que la santé physique, émotionnelle et spirituelle d'une personne s'améliore considérablement lorsque la relation avec la terre est rétablie dans sa vie. Elle préconise la création de nouveaux rituels qui favorisent ces relations. Les balades lunaires essaient d’être un nouveau rituel.

## Polycène
La psychologue et philosophe Vinciane Despret, connue pour ses livres «Autobiographie d'un Poulpe» et «Habiter en oiseau», propose de ne plus parler de l'Anthropocène, qui a une connotation dystopique, mais de Polyocène, la proposition de commencer à vivre à partir de l'idée que nous, les humains, ne vivons pas seuls sur cette planète. Nous faisons partie d'une polyphonie, et nous devons nous demander comment mieux vivre ensemble, et plus encore, créer ensemble avec les êtres plus qu'humains. Il est important de décentrer notre regard afin de pouvoir créer de telles relations. Vivre la présence de la forêt y contribue.

## Bruxelles
Ce ne sont là que deux des auteurs qui soulignent spécifiquement la fin de l'anthropocentrisme, mais aussi le bien-être personnel que nous pouvons développer si nous osons entrer en relation avec des êtres non humains. A Bruxelles, nous avons la chance d'avoir une grande forêt à notre disposition, la forêt de Soignes. En outre, il ne s'agit pas de n'importe quelle forêt. Elle a toujours été forêt depuis la dernière période glaciaire, il y a plus de 10 000 ans. Et même si les arbres ont été replantés par Zinner au XVIIIe siècle, la forêt conserve cette énergie particulière qui caractérise les lieux où les arbres sont les principaux habitants.

## Balades de pleine et nouvelle lune
Ces auteurs et d'autres m'ont inspiré l'idée d'organiser - comme un nouveau rituel - des promenades à la nouvelle et à la pleine lune. Je fais ceci depuis le printemps 2021. Ce sont des promenades à mi-chemin entre le bain de forêt, la méditation et les promenades guidées dans la nature.


<!--[quotes]-->

> J'ai aimé le mélange entre les différents éléments de la narration: entre biologie, fiction, politique... J'ai aussi beaucoup aimé le silence, et puis la densité de l'expérience. L'espace, par exemple, m'a semblé très dilaté (je connais bien l'endroit, je sais qu'on n'a pas marché pendant des kilomètres et pourtant ça m'a paru long, dense: un voyage). J'ai aussi beaucoup aimé voir/ sentir la nuit tomber, et me rendre compte que je n'avais pas peur. Parce que j'étais avec vous? Parce que les arbres m'avaient souhaité bienvenue avec bienveillance?
>
> Rafaella

<!--  -->

> Ce qu'il me reste comme souvenirs forts, ce sont les formes et les couleurs qui se métamorphosent progressivement le long de la balade, ainsi que les sons: ceux de nos pas en silence sur les graviers, et des oiseaux. Nous gardons tout.e.s les deux en nous l'intention que nous avons pu placer dans le moment dédié à la nouvelle lune, c'était fort et cela nous tient à cœur de la garder dans notre vie.
>
> Doriane & Zeste

<!--  -->

> Je sortais d'une journée très chargée et la promenade m'a calmée. Au début, j'avais du mal à me concentrer sur les invitations, parce que ma tête était encore en train de traiter toutes les impressions de la journée, mais vers la fin, j'étais complètement dedans.
>
> Delphine

<!--  -->

> Il y a quelque chose qui m'a touchée et qui me marque encore aujourd'hui mais je ne sais pas expliquer... sans doute d'avoir parlé avec les arbres, nos grands frères.
>
> Aicha
