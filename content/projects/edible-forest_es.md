Title: Bosque Comestible
slug: edible-forest
lang: es

En este paseo, descubrirás todo tipo de golosinas que se encuentran en la naturaleza, a lo largo de un sendero, en un árbol o junto a un arbusto. Desde las plantas más pequeñas hasta los árboles más grandes, en el bosque hay una abundancia de seres vivos listos a compartir lo que tienen de comestible y bebible. El bosque de Soignes/Zoniën es una zona protegida, por lo que no se puede recoger.

Pero abundancia hay!

## Guía
An Mertens es guía de naturaleza y artista. Ha organizado anteriormente paseos alrededor del Bosque Comestible para GC Wabo.

## Práctico
El paseo dura 2 horas y sale 20 euros.

Nos encontramos en el aparcamiento de Enfants Noyés/Verdronken Kinderen, en la Drève du Comte/Graafdreef en Boitsfort/Bosvoorde : [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Gracias por inscribirte con antelación en anais@anaisberck.be.


Fechas

Jueves 5 de septiembre, de 19.00 a 21.00 h

Jueves 19 de septiembre, de 18.00 a 20.00 h

Jueves 3 de octubre, de 17.00 a 19.00 h

Jueves 10 de octubre, de 17.00 a 19.00 h
