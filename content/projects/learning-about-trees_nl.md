Title: Bomen leren kennen
slug: learning-about-trees
lang: nl

Tijdens elk van deze wandelingen maak je kennis met vier verschillende bomen. Je leert kijken naar hun vorm, hun schors, hun bladeren en vruchten. Je ontdekt ook wat hun medicinale en spirituele krachten zijn en waarvoor ze zoal praktisch worden gebruikt.

Aan het einde van de wandeling heb je voldoende gereedschappen om zelf je eigen boomvrienden te ontmoeten.

## Gids
An Mertens is natuurgids en kunstenaar.

## Praktisch
Het is aangeraden om een reeks van 4 wandelingen te boeken, maar je kan ook voor elke wandeling apart inschrijven.

Een wandeling duurt 2 uur en kost 20€.

Een reeks van vier wandelingen kost 70€.

We spreken af op de parking van Verdronken Kinderen, in de Graafdreef in Bosvoorde: [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Graag op voorhand inschrijven via anais@anaisberck.be.


Data:

Reeks 1:
* woensdag 4 september, 18u-20u
* maandag 9 september, 18u-20u
* maandag 16 september, 18u-20u
* maandag 23 september, 18u-20u

Reeks 2:
* maandag 30 september, 17u-19u
* maandag 7 oktober, 17u-19u
* maandag 14 oktober, 17u-19u
* maandag 21 oktober, 17u-19u
