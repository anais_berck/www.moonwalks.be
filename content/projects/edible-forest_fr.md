Title: Forêt Comestible
slug: edible-forest
lang: fr

Au cours de cette promenade, vous découvrirez toutes sortes de bonnes choses que l'on trouve dans la nature, au détour d'un chemin, d'un arbre ou d'un buisson. Des plus petites plantes aux plus grands arbres, il y a une abondance d'êtres vivants dans la forêt qui sont prêts à partager du comestible et buvable. La forêt de Soignes est une zone protégée, la cueillette est donc exclue.

Mais l'abondance n'en est que plus grande !

## Guide
An Mertens est guide nature et artiste. Elle a déjà organisé des promenades autour de la forêt comestible pour GC Wabo.

## Pratique
La promenade dure 2 heures et coûte 20 €.

Nous nous retrouvons sur le parking de Enfants Noyés/Verdronken Kinderen, à la Drève du Comte/Graafdreef à Boitsfort/Bosvoorde : [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Veuillez vous inscrire à l'avance sur anais@anaisberck.be.

Dates

Jeudi 5 septembre, 19h-21h

Jeudi 19 septembre, 18h-20h

Jeudi 3 octobre, 17h-19h

Jeudi 10 octobre, 17h-19h
