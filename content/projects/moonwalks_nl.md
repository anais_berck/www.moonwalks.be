Title: Maanwandelingen
slug: moonwalks
lang: nl


<!--[main]-->

## Nieuwe rituelen
Botaniste en Native American Indian Robin Wall Kimmerer stelt in haar boek “Braiding Sweetgrass”, dat de kern van de klimaatcrisis te maken heeft met het verliezen van de verbinding met het land. Door de verbinding weer aan te gaan, kan er ook een vorm van herstel van het land en de mensen plaatsvinden en een duurzaam zorg-dragen-voor. De verbinding kan op verschillende manieren gemaakt worden, bijvoorbeeld door de verantwoordelijkheid te nemen voor een stuk grond, zij het als moestuin, siertuin of bos. Haar hypothese is dat zowel de fysieke, de emotionele als de spirituele gezondheid van een persoon sterk verbetert als de relatie met het land weer een plaats krijgt in zijn/haar leven. Ze dringt erop aan om nieuwe rituelen in het leven te roepen, die die relaties bevorderen. De maanwandelingen proberen om zo’n nieuw ritueel te zijn.

## Polyceen
Psychologe en filosofe Vinciane Despret, bekend van haar boeken “Autobiographie d'un Poulpe” en “Habiter en oiseau” stelt voor om niet meer te spreken van de Antropoceen, die een dystopische connotatie in zich draagt, maar van de Polyceen, het voorstel om te gaan leven vanuit de idee dat we als mensen niet alleen op deze planeet leven. We zijn deel van een meerstemmigheid, en we moeten ons afvragen hoe we beter kunnen samenleven, en meer nog, samen creëren met de meer-dan-menselijke wezens. Het is belangrijk om onze blik te decentreren, zodat we zo'n relaties kunnen aangaan. Het bos helpt daarbij.

## Brussel
Dit zijn maar twee van de auteurs die specifiek wijzen op het einde van het antropocentrisme, en ook op het persoonlijke welzijn dat we kunnen ontwikkelen als we in relatie durven gaan met andere dan menselijke wezens. In Brussel hebben we het geluk van een groot bos ter beschikking te hebben, het Zoniënwoud. Bovendien is het niet zomaar een bos. De ondergrond is sinds de laatste ijstijd meer dan 10000 jaar geleden altijd bos geweest. En ook al zijn de bomen heraangeplant door Zinner in de 18de eeuw, toch blijft het bos die speciale energie behouden die plekken kenmerkt, waar bomen de belangrijkste bewoners zijn.

## Volle en Nieuwe Maanwandelingen
Deze en andere auteurs inspireerden me om – als een nieuw ritueel - nieuwe en volle maanwandelingen te organiseren. Ik doe dit sinds de lente van 2021. Het zijn wandelingen die het midden houden tussen bosbaden, meditatie en geleide natuurwandelingen.


<!--[quotes]-->

> Ik heb genoten van de mix van verschillende ingrediënten: biologie, fictie, politiek... Ik hield ook van de stilte, en de intensiteit van de ervaring. De ruimte, bijvoorbeeld, leek mij heel uitgestrekt (ik ken de plaats goed, ik weet dat wij geen kilometers hebben gelopen en toch leek het lang, intens: een reis). Ik vond het ook heel fijn om de nacht te zien/voelen verschijnen, en te beseffen dat ik niet bang was. Omdat ik bij jou was? Omdat de bomen me vriendelijk welkom hadden geheten?
>
> Rafaella

<!--  -->

> Wat ik als sterke herinneringen heb overgehouden, zijn de vormen en kleuren die veranderden tijdens de wandeling, en ook de geluiden: onze voetstappen in stilte op het grind, en de vogels. We koesteren allebei de intentie die we plaatsten tijdens het moment dat je wijdde aan de nieuwe maan. Dat was sterk. We vinden het belangrijk om dat levendig te houden.
>
> Doriane & Zeste

<!--  -->

> Ik kwam uit een heel drukke dag en de wandeling heeft me terug rustig gemaakt. In het begin had ik moeite om me te concentreren op wat de opdrachten waren, omdat m’n hoofd nog alle indrukken van de dag aan het verwerken was, maar naar het einde toe zat ik er helemaal in.
>
> Delphine

<!--  -->

> Er was iets dat me geraakt heeft en dat nu nog steeds doet, maar ik weet niet hoe het uit te leggen... het heeft zeker te maken met de communicatie met de bomen, onze grote broers.
>
> Aicha
