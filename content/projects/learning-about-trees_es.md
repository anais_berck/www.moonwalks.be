Title: Reconocer a los Arboles
slug: learning-about-trees
lang: es

Durante cada uno de estos paseos, conocerás cuatro a árboles diferentes. Aprenderás a fijarte en su forma, su corteza, sus hojas y sus frutos. También descubrirás cuáles son sus poderes medicinales y espirituales y para qué se utilizan en la vida práctica.

Al final del paseo, tendrás herramientas suficientes para hacerte tus propixs árboles-amigxs.

## Guía
An Mertens es guía de naturaleza y artista.

## Práctico
Se recomienda reservar una serie de 4 paseos, pero también puedes inscribirte en cada paseo por separado.

Un paseo dura 2 horas y sale 20€.

Una serie de cuatro paseos sale 70 euros.

Nos encontramos en el aparcamiento de Enfants Noyés/Verdronken Kinderen, en Drève du Comte/Graafdreef en Boitsfort/Bosvoorde: [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Gracias por inscribirte via anais@anaisberck.be.

Fechas:

Serie 1:
* Miércoles 4 de septiembre, de 18.00 a 20.00 h
* Lunes 9 de septiembre, de 18.00 a 20.00 h
* Lunes 16 de septiembre, de 18.00 a 20.00 h
* Lunes 23 de septiembre, de 18.00 a 20.00 h

Serie 2:
* Lunes 30 de septiembre, de 17.00 a 19.00 h
* Lunes 7 de octubre, de 17.00 a 19.00 h
* Lunes 14 de octubre, de 17.00 a 19.00 h
* Lunes 21 de octubre, de 17.00 a 19.00 h
