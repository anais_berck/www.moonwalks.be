Title: Learning about Trees
slug: learning-about-trees

During each of these walks, you will get to know four different trees. You will learn to look at their shape, their bark, their leaves and fruits. You will also discover what their medicinal and spiritual powers are and what they are practically used for.

At the end of the walk, you will have enough tools to meet your own tree friends.

## Guide
An Mertens is a nature guide and artist.

## Practical
It is recommended to book a series of 4 walks, but you can also register for each walk separately.

One walk lasts 2 hours and costs 20€.

A series of four walks costs 70€.

We meet at the parking lot of Enfants Noyés/Verdronken Kinderen, in Drève du Comte/Graafdreef in Boisfort/Bosvoorde: [https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096](https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096).

Please register in advance at anais@anaisberck.be.


Dates:

Series 1:
* Wednesday 4 September, 6pm-8pm
* Monday 9 September, 6pm-8pm
* Monday 16 September, 6pm-8pm
* Monday 23 September, 6pm-8pm

Series 2:
* Monday 30 September, 5pm-7pm
* Monday 7 October, 5pm-7pm
* Monday 14 October, 5pm-7pm
* Monday 21 October, 5pm-7pm
