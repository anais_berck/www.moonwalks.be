
# Moonwalks.be

## Editing content

The content files are in the `content/` directory, in the form of `markdown` files, in different languages.

The `content/calendar` directory contains one `yaml` file with the currently planned _walks_, see next session on how to edit.

The `content/projects` directory contains the texts presentation of the different projects. 

The `content/pages` directory contains the other website pages: the index/bio/intro, the gallery (empty because there is no text).

Images can be dropped in the `content/gallery` folder, name as no importance, a plugin is going to resize them and sort them by hours of the day they were taken (using their metadata).

## Note on language

The project's filenames need to contain only one `_`, separating the title from the language.

Furthermore inside every file we need to precise a `slug:` and a `lang:`, the `slug` is used as an id that match every translated pages (it often is a simplified version of the title), so they need to have the exact same slug to be recognised as translations. The `lang` can be `en` (default if not precised), `fr`, `nl`, `es`.

## Adding walk entries

The moonwalk calendar is a `yaml` file, in `content/calendar/`.
To add a new entry: 
    
1. open the website in a browser and go to the calendar to see what are the next New/Full moon dates
2. either copy the date by hand (with `-` between 2-digit numbers) or click anything on the related date box on the calendar webpage (it will automatically copy the data)
3. add a new entry in the `yaml` file by pasting the date (at one more indentation level than the `calendar:` key)
4. fill with the other fields like so:

```yaml
  calendar:

    #New Moon walk on the 30.04.22
    30-04-22:
      event_type: 
        en: "Walk"
        fr: "Ballade"
        nl: "Wandelen"
        es: "Paseos"
      start_time: "20:00"
      start_location: "Drève du comte"
      start_location_link: "https://www.openstreetmap.org/way/14502214#map=17/50.79518/4.40096"

```

Everytime the webpage load it will first get the New/Full moon dates from the API, then see if any entries in the calendar matches one of those date and if it does, add the corresponding informations to it.

## Regenerate the website after editing content

### Init steps

1. clone the repository
2. install requirements `pip install -r requirements.txt`
3. install lftp `sudo apt install lftp`
4. create a file with name `.env` in the root folder containing the `ftp` logs, like this
```bash
FTP_HOST=""
FTP_USERNAME=""
FTP_PASSWORD=""
```

### For every updates of the website

1. make your edits to the `content/` folder
2. `add`, `commit` and `push` your changes to the directory
3. open a terminal in the root folder of this project and launch the `pelican` command to regenerate
4. launch `./sync.sh`

If a change don't appear immediatly it's possible you have to hard refresh using `ctrl`+`shift`+`r`.


<!-- ## Notes

* if location changes to another country it is possible that the previous walk doesn't correspond to their yaml (because the hour shift, make it jump one day before or after)
  
* we could do an automatic newsletter or RSS flux ? 

* testing other region in calendar:
  # -- test for california
  # latitude: 36.7783
  # longitude: -119.4179
  # timezone: "America/Los_Angeles"
  # https://www.timeanddate.com/moon/phases/usa/california-city
  # it works !!
  
  -->