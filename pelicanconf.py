#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Anaïs Berck'
SITENAME = 'moonwalks.be'
SITEURL = ''

PATH = 'content'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# CUSTOM PAREMETERS
# ==============================================================================

# a page work like that 1 md file = 1 template
# an article for like that


# theme path
THEME = 'theme'


# ARCHITECTURE
# ==============================================================================

# 3 categories (calendar, projects, gallery)
# those categories serves as navigation (in base.html):
#    - iterate through the list to generate nav menu
#    - put as active if current article category is cat menu item
# every category is exactly one template
USE_FOLDER_AS_CATEGORY = True

ARTICLE_PATHS = ['projects/']   # exclude articles outside of this
# however we add the metadata "save_as: '' " to the calendar/ one
# so they're available through the articles variable, but not saved as independant file
# (status: hidden) does the opposite (save the page, but unlist it)
ARTICLE_SAVE_AS = '{category}/{slug}.html'
ARTICLE_URL = '/{category}/{slug}.html'
ARTICLE_LANG_SAVE_AS = '{category}/{slug}_{lang}.html'
ARTICLE_LANG_URL = '/{category}/{slug}_{lang}.html'

# pages have no category
PAGE_PATHS = ['pages/', 'flyer/'] 
PAGE_SAVE_AS = '{slug}.html'
PAGE_URL = '/{slug}.html'
PAGE_LANG_SAVE_AS = '{slug}_{lang}.html'
PAGE_LANG_URL = '/{slug}_{lang}.html'
SLUGIFY_SOURCE = 'title'

CATEGORY_URL = '{slug}/'

# disable the generation of those default html files
CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''

# the "collections" type html pages that we want
# DIRECT_TEMPLATES = ['index', 'authors', 'categories', 'tags', 'archives']
# because gallery and even index is a page
# (this allow to have translation for index_fr, index_nl, etc)
DIRECT_TEMPLATES = []

# to copy every image to the output direction
STATIC_PATHS = ['gallery']

MENUITEMS = [
    # ("calendar", "/index.html"),
]


# TRANSLATION
# ==============================================================================

# only article in this lang will be listed by default
DEFAULT_LANG = 'en'
# two article with same slug are considered as translation:
# ARTICLE_TRANSLATION_ID = 'slug'
# by default slug is generated from title:
# SLUGIFY_SOURCE = 'title'
# which means the translated article have to have the same title
# but the title is going to be translated, so by default
# the best way is to setup slug metadata manually so it fit the english slugification

PAGE_TRANSLATION_ID = 'slug'

# DATE
# ==============================================================================

TIMEZONE = 'Europe/Paris'
# by default pelican asks for a date for every article (?)
DEFAULT_DATE = 'fs'

# PLUGINS
# ==============================================================================

PLUGIN_PATHS = ['plugins']

PLUGINS = ['html-comment-sections', 'data_files', 'gallery','image_process','multilang-json-page']

# --- quotes-sections (self made)
# separate an article when meeting <!--quotes-->
# put the first section in the article.content variable
# put the second section in the article.quotes variable
# TODO: generalize it for every short text in comment as different "section"

# --- data_files (from: https://github.com/LucasVanHaaren/pelican-data-files, with custom added YAML support)
# by parse the json and yaml file in the data/ directory and add them tp the context
# as object with name "DATA_[filename]"
DATA_FILES_DIR = "calendar/"

# --- gallery
# create a GALLERY global variable list of urls for img contained in the /gallery/ folder
# and parse their exifs data to add the date on which they were taken

# --- image_process (from: https://github.com/pelican-plugins/image-process)
# note: we can add the class manually in the html template (and not in the markdown)
IMAGE_PROCESS = {
    # reminder on srctset
    # https://www.youtube.com/watch?v=2QYpkrX2N48&ab_channel=KevinPowell
    # our gallery <img> html element varies between
    # 300 and 500 browser pixels width
    # so on 1x devices we give it a 500px image to be sure
    "gallery": {
        "type": "responsive-image",
        # Here we could actually precise the size of the <img> html element with media query
        # then we have to replace the 1x, 2x with actual px width of the image element
        # "sizes": (
        #     "500px"
        # ),
        "srcset": [
            # first parameter is to inform the browser of the image file size (with the w unit)
            # or just precising the display subpixel type
            # !before! it has to actually download the image
            ("1x", ["scale_in 500 500 True"]),
            ("2x", ["scale_in 1000 1000 True"])
        ],
        "default": "1x"
    }

}